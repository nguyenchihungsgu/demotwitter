package co.rockship.demo.common.bases;

import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity<M extends BaseViewModel, B extends ViewDataBinding> extends DaggerAppCompatActivity {

    protected B viewDataBinding;
    @Inject
    protected M viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutResId());
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClass());
        viewDataBinding.setLifecycleOwner(this);
        onCreate(savedInstanceState, viewModel, viewDataBinding);
    }

    protected abstract Class<M> getViewModelClass();

    protected abstract void onCreate(Bundle instance, M viewModel, B binding);

    protected abstract
    @LayoutRes
    int getLayoutResId();

    public B getViewDataBinding() {
        return viewDataBinding;
    }

    public M getViewModel() {
        return viewModel;
    }
}
