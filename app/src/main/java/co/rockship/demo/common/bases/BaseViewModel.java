
package co.rockship.demo.common.bases;

import androidx.lifecycle.ViewModel;
import co.rockship.demo.common.utils.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseViewModel extends ViewModel {

    private final SchedulerProvider schedulerProvider;

    private final CompositeDisposable compositeDisposable;

    public BaseViewModel(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    protected SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }
}
