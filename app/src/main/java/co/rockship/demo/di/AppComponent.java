package co.rockship.demo.di;


import javax.inject.Singleton;

import co.rockship.demo.DemoTwitterApplication;
import co.rockship.demo.di.modules.AppModule;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class
})
public interface AppComponent extends AndroidInjector<DemoTwitterApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<DemoTwitterApplication> {
    }
}
