package co.rockship.demo.di;

import co.rockship.demo.features.launch.LaunchActivity;
import co.rockship.demo.features.launch.LaunchActivityModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = {LaunchActivityModule.class})
    abstract LaunchActivity bindLaunchActivity();
}
