package co.rockship.demo.di.modules;


import com.twitter.sdk.android.core.TwitterSession;

import javax.inject.Singleton;

import co.rockship.demo.data.sources.SearchApi;
import co.rockship.demo.data.sources.SearchRemoteDataSource;
import co.rockship.demo.data.sources.SearchRepository;
import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {
    @Provides
    @Singleton
    SearchRepository provideUserDataRepository(SearchApi api, TwitterSession session) {
        return new SearchRepository(new SearchRemoteDataSource(api,session));
    }
}
