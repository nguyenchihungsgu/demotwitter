package co.rockship.demo.di.modules;

import android.content.Context;

import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import javax.inject.Singleton;

import co.rockship.demo.DemoTwitterApplication;
import co.rockship.demo.common.utils.AppSchedulerProvider;
import co.rockship.demo.common.utils.SchedulerProvider;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
        RestfulModule.class,
        RepositoryModule.class
})
public class AppModule {

    @Provides
    @Singleton
    Context provideApplicationContext(DemoTwitterApplication application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    TwitterSession provideTwitterSession() {
        return TwitterCore.getInstance().getSessionManager().getActiveSession();
    }
}
