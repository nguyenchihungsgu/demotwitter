package co.rockship.demo.features.launch;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import co.rockship.demo.R;
import co.rockship.demo.common.bases.BaseActivity;
import co.rockship.demo.databinding.ActivityLaunchBinding;

public class LaunchActivity extends BaseActivity<LaunchViewModel,ActivityLaunchBinding>{
    @Override
    protected Class<LaunchViewModel> getViewModelClass() {
        return LaunchViewModel.class;
    }

    @Override
    protected void onCreate(Bundle instance, LaunchViewModel viewModel, ActivityLaunchBinding binding) {
        getViewDataBinding().loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                Toast.makeText(LaunchActivity.this,"Login success - token: " + session.getAuthToken().token,Toast.LENGTH_LONG).show();
                getViewModel().setSearchByUserId(127159535);
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                exception.printStackTrace();
            }
        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_launch;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getViewDataBinding().loginButton.onActivityResult(requestCode, resultCode, data);
    }

}
