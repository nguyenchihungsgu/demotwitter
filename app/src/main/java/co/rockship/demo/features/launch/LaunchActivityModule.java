package co.rockship.demo.features.launch;

import co.rockship.demo.common.utils.SchedulerProvider;
import co.rockship.demo.data.sources.SearchRepository;
import co.rockship.demo.di.modules.ViewModelFactory;
import dagger.Module;
import dagger.Provides;

@Module
public class LaunchActivityModule {

    @Provides
    LaunchViewModel provideSplashViewModel(SchedulerProvider schedulerProvider, SearchRepository searchRepository) {
        return new LaunchViewModel(schedulerProvider,searchRepository);
    }

    @Provides
    androidx.lifecycle.ViewModelProvider.Factory provideVMFactory(LaunchViewModel viewModel) {
        return new ViewModelFactory<>(viewModel);
    }
}
