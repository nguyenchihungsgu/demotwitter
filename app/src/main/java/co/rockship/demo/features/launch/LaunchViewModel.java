package co.rockship.demo.features.launch;

import android.util.Log;

import co.rockship.demo.common.bases.BaseViewModel;
import co.rockship.demo.common.utils.SchedulerProvider;
import co.rockship.demo.data.sources.SearchRepository;

public class LaunchViewModel extends BaseViewModel {

    public void setOutput(Output output) {
        this.output = output;
    }

    interface Output {
        void onSuccess();
    }

    private SearchRepository searchRepository;
    private Output output;

    LaunchViewModel(SchedulerProvider schedulerProvider, SearchRepository searchRepository) {
        super(schedulerProvider);
        this.searchRepository = searchRepository;
    }

    public void setSearchByUserId(long userId){
        getCompositeDisposable().add(searchRepository.getTimeLine(userId)
            .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(twitterRespond -> Log.d("Success",twitterRespond.toString()), throwable -> throwable.printStackTrace())
        );
    }
}
