package co.rockship.demo.data.sources;

import co.rockship.demo.data.models.TwitterRespond;
import io.reactivex.Flowable;

public interface SearchContract {
    Flowable<TwitterRespond> getTimeLine(long userId);
}
