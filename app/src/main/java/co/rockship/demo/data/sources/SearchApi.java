package co.rockship.demo.data.sources;

import co.rockship.demo.data.models.TwitterRespond;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchApi {
    @GET("1.1/statuses/user_timeline.json")
    Flowable<TwitterRespond> getTimeLine(@Query("user_id") long userId);
}
