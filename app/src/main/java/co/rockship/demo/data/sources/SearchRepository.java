package co.rockship.demo.data.sources;

import co.rockship.demo.data.models.TwitterRespond;
import io.reactivex.Flowable;

public class SearchRepository implements SearchContract {

    private final SearchContract remote;

    public SearchRepository(SearchContract remote) {
        this.remote = remote;
    }

    @Override
    public Flowable<TwitterRespond> getTimeLine(long userId) {
        return remote.getTimeLine(userId);
    }
}