package co.rockship.demo.data.sources;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

import co.rockship.demo.data.models.TwitterRespond;
import io.reactivex.Flowable;

public class SearchRemoteDataSource extends TwitterApiClient implements SearchContract {

    private final SearchApi api;

    public SearchRemoteDataSource(SearchApi api,TwitterSession session) {
        super(session);
        this.api = api;
    }

    @Override
    public Flowable<TwitterRespond> getTimeLine(long userId) {
        return api.getTimeLine(userId);
    }
}